#ifndef EIGHTQUEENS_OBJLOADER_H
#define EIGHTQUEENS_OBJLOADER_H

#include <GL\glut.h>
#include <vector>


struct vertex
{
    vertex(){};
    vertex(double x, double y, double z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    double x, y, z;
};

struct color4
{
    color4(float r, float g, float b, float a)
    {
        this->r = r;
        this->g = g;
        this->b = b;
        this->a = a;
    }

    float r, g, b, a;
};

class OBJLoader {
public:
    OBJLoader();
    OBJLoader(const char *filename);

    vertex getPosition() const { return position; }

    void setPosition(vertex pos);
    void setColor(color4 color);
    void readFile(const char *filename);
    void initBuffers();
    void render();

    int objectId;
    GLuint verticesBufferId, normalsBufferId, facesBufferId, colorsBufferId;

private:
    std::vector<vertex> vertices;
    std::vector<GLuint> faces;
    std::vector<vertex> normals;
    std::vector<color4> colors;
    vertex position;
};

#endif //EIGHTQUEENS_OBJLOADER_H
