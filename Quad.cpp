#include <GL/glew.h>
#include "Quad.h"

Quad::Quad(){};

Quad::Quad(vertex v1, vertex v2, vertex v3, vertex v4, vertex center_pos, color4 color, color4 initColor, int name)
{
    this->initColor = initColor;
    std::vector<vertex> vertices;
    vertices.reserve(4);
    vertices.push_back(v1);
    vertices.push_back(v2);
    vertices.push_back(v3);
    vertices.push_back(v4);

    this->vertices = vertices;

    std::vector<GLuint> face;
    face.reserve(4);
    face.push_back(0);
    face.push_back(1);
    face.push_back(2);
    face.push_back(3);

    this->faces = face;

    std::vector<vertex> normals;
    normals.reserve(4);
    normals.emplace_back(0.0, 1.0, 0.0);
    normals.emplace_back(0.0, 1.0, 0.0);
    normals.emplace_back(0.0, 1.0, 0.0);
    normals.emplace_back(0.0, 1.0, 0.0);

    this->normals = normals;

    std::vector<color4> colors;
    colors.reserve(4);
    colors.push_back(color);
    colors.push_back(color);
    colors.push_back(color);
    colors.push_back(color);

    this->color = colors;

    this->id = name;

    this->center_pos = center_pos;

    Quad::createVBO();
}

void Quad::createVBO()
{
    glGenBuffers(1, &vertexBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vertex), &vertices[0], GL_STATIC_DRAW);

    glGenBuffers(1, &colorBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, colorBufferId);
    glBufferData(GL_ARRAY_BUFFER, color.size() * sizeof(color), &color[0], GL_DYNAMIC_DRAW);

    glGenBuffers(1, &normalBufferId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, normalBufferId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, normals.size() * sizeof(vertex), &normals[0], GL_STATIC_DRAW);

    glGenBuffers(1, &faceBufferId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, faceBufferId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, faces.size() * sizeof(GLuint), &faces[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Quad::render()
{
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
    glVertexPointer(3, GL_DOUBLE, 0, NULL);

    glBindBuffer(GL_ARRAY_BUFFER, colorBufferId);
    glColorPointer(4, GL_FLOAT, 0, NULL);


    glLoadName(this->id);

    glBindBuffer(GL_ARRAY_BUFFER, normalBufferId);
    glNormalPointer(GL_DOUBLE, 0, NULL);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, faceBufferId);
    glDrawElements(GL_QUADS, (faces.size() * sizeof(GLuint)) / 4, GL_UNSIGNED_INT, NULL);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Quad::setColor(color4 quadColor) {
    std::vector<color4> colors;
    colors.reserve(4);
    colors.push_back(quadColor);
    colors.push_back(quadColor);
    colors.push_back(quadColor);
    colors.push_back(quadColor);

    this->color = colors;


    glBindBuffer(GL_ARRAY_BUFFER, colorBufferId);
    glBufferData(GL_ARRAY_BUFFER, color.size() * sizeof(color), &color[0], GL_DYNAMIC_DRAW);

}
