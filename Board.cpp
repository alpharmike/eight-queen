#include "Board.h"
#include <GL/glut.h>

Board::Board(){};

Board::Board(std::vector<Quad*> quads)
{
    this->quads = quads;
}

void Board::init(GLfloat vertices[]) {
    QUAD_COLOR currRowQuadColor = QUAD_COLOR::BLACK;
    QUAD_COLOR currColQuadColor = QUAD_COLOR::BLACK;

    for (int i = 0; i < BOARD_WIDTH - 1; i++) {
        auto src = i * BOARD_WIDTH;
        auto dest = (i + 1) * BOARD_WIDTH;

        double x, y, z;

        for (int j = 0; j < BOARD_HEIGHT - 1; j++) {
            if (j == 0) currColQuadColor = currRowQuadColor;
            vertex vert1 = vertex(vertices[src * 3], vertices[src * 3 + 1], vertices[src * 3 + 2]);
            vertex vert2 = vertex(vertices[dest * 3], vertices[dest * 3 + 1], vertices[dest * 3 + 2]);
            src += 1;
            dest += 1;
            vertex vert3 = vertex(vertices[dest * 3], vertices[dest * 3 + 1], vertices[dest * 3 + 2]);
            vertex vert4 = vertex(vertices[src * 3], vertices[src * 3 + 1], vertices[src * 3 + 2]);

            x = vertices[(i * DIM + j) * 3] + 0.5;
            y = 0;
            z = vertices[(i * DIM + j) * 3 + 2] + 0.5;

            if (currColQuadColor == QUAD_COLOR::BLACK) {
                Quad *square = new Quad(vert1, vert2, vert3, vert4, vertex(x, y, z), color4(0.0, 0.0, 0.0, 1.0), color4(0.0, 0.0, 0.0, 1.0), 8 + (i * (BOARD_WIDTH - 1) + j)); // consider a change of 2: 10
                square->row = i;
                square->col = j;
                quads.push_back(square);
            }
            else {
                Quad *square = new Quad(vert1, vert2, vert3, vert4, vertex(x, y, z), color4(1.0, 1.0, 1.0, 1.0), color4(1.0, 1.0, 1.0, 1.0), 8 + (i * (BOARD_WIDTH - 1) + j)); // consider a change of 2: 10
                square->row = i;
                square->col = j;
                quads.push_back(square);
            }

            currColQuadColor = currColQuadColor == QUAD_COLOR::BLACK ? QUAD_COLOR::WHITE : QUAD_COLOR::BLACK;
        }
        currRowQuadColor = currRowQuadColor == QUAD_COLOR::BLACK ? QUAD_COLOR::WHITE : QUAD_COLOR::BLACK;

    }
}

void Board::render()
{
    for (Quad *quad : quads)
    {
        glPushMatrix();
        glLoadName(quad->id);
        quad->render();
        glPopMatrix();
    }
}
