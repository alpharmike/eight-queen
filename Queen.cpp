#include "Queen.h"
#include <GL/glut.h>

Queen::Queen() {

}

Queen::Queen(int row, int col, int name, vertex pos) : Queen() {
    this->object = OBJLoader("./models/dame.obj");
    this->object.setPosition(pos);
    this->object.setColor(color4(0.0, 0.0, 0.0, 1.0));
    this->object.objectId = name;
    this->object.setColor(color4(0.0, 0.0, 0.0, 1.0));

    this->row = row;
    this->col = col;

    this->inConflict = false;

    this->selected = false;
}

void Queen::setSelected(bool isSelected) {
    this->selected = isSelected;

    if (this->selected) {
        this->object.setColor(color4(1.0, 1.0, 0.0, 1.0));
    } else {
        this->object.setColor(color4(0.0, 0.0, 0.0, 1.0));
    }
}

void Queen::render() {
    glPushMatrix();
    glLoadIdentity();
    glTranslatef(object.getPosition().x, object.getPosition().y, object.getPosition().z);
    glScalef(0.015, 0.015, 0.015); // check with 0.01
    glLoadName(object.objectId);
    object.render();
    glPopMatrix();
}

MovementInfo Queen::move(Queen *selectedQueen, Quad *targetQuad) {
    /**
     * @details: the functions calculates the distance between the position of the selected queen and the target quad,
     * and sets its y position to a higher value than the current quad. It sets the row and column of the queen to the target quad.
     * @param selectedQueen: the queen to be moved
     * @param targetQuad: the quad to be placed at
     */
    double distance, movePerFrameX, movePerFrameZ;

    if (targetQuad->row == selectedQueen->row) {

        distance = abs(targetQuad->col - selectedQueen->col);
        movePerFrameZ = (targetQuad->center_pos.z - selectedQueen->object.getPosition().z) / distance;
        movePerFrameX = 0;

    } else if (targetQuad->col == selectedQueen->col) {
        distance = abs(targetQuad->row - selectedQueen->row);
        movePerFrameX = (targetQuad->center_pos.x - selectedQueen->object.getPosition().x) / distance;
        movePerFrameZ = 0;
    } else {
        distance = abs(targetQuad->row - selectedQueen->row);
        movePerFrameX = (targetQuad->center_pos.x - selectedQueen->object.getPosition().x) / distance;
        movePerFrameZ = (targetQuad->center_pos.z - selectedQueen->object.getPosition().z) / distance;
    }

    selectedQueen->object.setPosition(
            vertex(selectedQueen->object.getPosition().x, selectedQueen->object.getPosition().y + 1.5,
                   selectedQueen->object.getPosition().z));

    selectedQueen->row = targetQuad->row;
    selectedQueen->col = targetQuad->col;

    return MovementInfo(distance, movePerFrameX, movePerFrameZ);
}

void Queen::resetColor() {
    this->object.setColor(color4(0.0, 0.0, 0.0, 1.0));
}

void Queen::setInConflict(bool isConflicted) {
    if (isConflicted) {
        this->inConflict = true;
        this->object.setColor(color4(1.0, 0.0, 0.0, 1.0));
    } else {
        this->inConflict = false;
        this->object.setColor(color4(0.0, 0.0, 0.0, 1.0));
    }
}