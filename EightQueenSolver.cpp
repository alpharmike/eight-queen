#include "EightQueenSolver.h"

using namespace std;

bool EightQueenSolver::checkForEnd(int queen_positions[DIMENSION][DIMENSION], int col) {
    if (col >= DIMENSION) //when N queens are placed successfully
        return true;
    for (int i = 0; i < DIMENSION; i++) { //for each row, check placing of queen is possible or not
        if (isValid(queen_positions, i, col)) {
            if (checkForEnd(queen_positions, col + 1)) //Go for the other columns recursively
                return true;
        }
    }
    return false; //when no possible order is found
}

bool EightQueenSolver::isValid(int queen_positions[DIMENSION][DIMENSION], int row, int col) {
    for (int i = 0; i < DIMENSION; ++i) {
        if (queen_positions[i][col] && i != row) {
            return false;
        }
    }
    for (int i = 0; i < col; i++) //check whether there is queen in the left or not
        if (queen_positions[row][i]) {
            return false;
        }
    for (int i = row - 1, j = col - 1; i >= 0 && j >= 0; i--, j--)
        if (queen_positions[i][j]) {
            return false;

        } //check whether there is queen in the left upper diagonal or not
    for (int i = row + 1, j = col - 1; j >= 0 && i < DIMENSION; i++, j--)
        if (queen_positions[i][j]) {
            return false;

        } //check whether there is queen in the left lower diagonal or not
    return true;
}