#ifndef EIGHTQUEENS_QUAD_H
#define EIGHTQUEENS_QUAD_H

#include <vector>
#include <GL/glut.h>
#include "OBJLoader.h"

class Quad
{
public:
    Quad();
    Quad(vertex v1, vertex v2, vertex v3, vertex v4, vertex center_pos, color4 color, color4 initColor, int id);

    void createVBO();
    void render();
    void setColor(color4 quadColor);

    int id;
    int row;
    int col;
    color4 initColor = color4(0, 0, 0, 0);
    vertex center_pos;
    GLuint vertexBufferId, faceBufferId, normalBufferId, colorBufferId;
private:
    std::vector<vertex> vertices;
    std::vector<GLuint> faces;
    std::vector<vertex> normals;
    std::vector<color4> color;
};

#endif //EIGHTQUEENS_QUAD_H
