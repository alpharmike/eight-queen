#include "Camera.h"


Camera::Camera() {
    rotatingLeft = false;
    rotatingRight = false;
    zoomingIn = false;
    zoomingOut = false;
}

Camera::Camera(int w, int h, float camera_theta, float fov) : Camera() {
    this->width = w;
    this->height = h;
    this->camera_theta = camera_theta;
    this->fov = fov;

    rotatingLeft = false;
    rotatingRight = false;
    zoomingIn = false;
    zoomingOut = false;

}

void Camera::set_camera_config(int w, int h, float camera_theta, float fov) {
    /**
     * @details: setting perspective camera configurations
     * @param w: specifies the width of the window
     * @param h: specifies the height of the window
     * @param camera_theta: specifies the rotation angle of the camera
     * @param fov: specifies the field of view angle
     */
    this->width = w;
    this->height = h;
    this->camera_theta = camera_theta;
    this->fov = fov;

    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(fov, (float) w / (float) h, 2.0, 500.0);


    gluLookAt(
            15, 10, 20,
            0, 0, 0,
            0, 1, 0);

    glRotatef(camera_theta, 0, 1, 0);
}




