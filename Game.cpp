#include <GL/glew.h>
#include "Game.h"
#include <iostream>
#include <vector>
#include "Queen.h"
#include "Board.h"
#include "stb_image.h"
#include <glm.hpp>
#include<ctime>
#include <cmath>
#include "Shader.h"
#include <gtc/type_ptr.hpp>
#include <gtx/string_cast.hpp>
#include <irrKlang/irrKlang.h>

#define BUFFER_CAPACITY 512

using namespace irrklang;

ISoundEngine *soundEngine;

// camera perspective parameters
float camera_fov = 40.0;
float camera_theta = 0.0;

// queen movement info
bool movement_started = false;
double moveLengthX = 0;
double moveLengthZ = 0;

// time of picking queen
int touchTime;

// queen selected for movement
Queen *selectedQueen;

// current moving queen info
MovementInfo movementInfo;

Quad* targetQuad;

// list of all queens
std::vector<Queen *> queens;

Board board;

// board quads vertices
GLfloat board_vertices[BOARD_HEIGHT * BOARD_WIDTH * 3];

Camera *camera;

// set game configs
bool hintEnabled = false;
bool fullScreen = false;

int queen_positions[DIMENSION][DIMENSION];

unsigned int skyboxVAO, skyboxVBO;
unsigned int cubemapTexture;

unsigned int Game::loadCubemap(std::vector<std::string> faces)
{

    glEnable(GL_TEXTURE_CUBE_MAP);

    unsigned int textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

    int width, height, nrComponents;
    for (unsigned int i = 0; i < faces.size(); i++)
    {
        unsigned char *data = stbi_load(faces[i].c_str(), &width, &height, &nrComponents, 0);
        if (data)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
            stbi_image_free(data);
        }
        else
        {
            std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
            stbi_image_free(data);
        }
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    return textureID;
}

void Game::initSkybox() {
    float skyboxVertices[] = {
            -4000.0f, 4000.0f, -4000.0f,
            -4000.0f, -4000.0f, -4000.0f,
            4000.0f, -4000.0f, -4000.0f,
            4000.0f, -4000.0f, -4000.0f,
            4000.0f, 4000.0f, -4000.0f,
            -4000.0f, 4000.0f, -4000.0f,

            -4000.0f, -4000.0f, 4000.0f,
            -4000.0f, -4000.0f, -4000.0f,
            -4000.0f, 4000.0f, -4000.0f,
            -4000.0f, 4000.0f, -4000.0f,
            -4000.0f, 4000.0f, 4000.0f,
            -4000.0f, -4000.0f, 4000.0f,

            4000.0f, -4000.0f, -4000.0f,
            4000.0f, -4000.0f, 4000.0f,
            4000.0f, 4000.0f, 4000.0f,
            4000.0f, 4000.0f, 4000.0f,
            4000.0f, 4000.0f, -4000.0f,
            4000.0f, -4000.0f, -4000.0f,

            -4000.0f, -4000.0f, 4000.0f,
            -4000.0f, 4000.0f, 4000.0f,
            4000.0f, 4000.0f, 4000.0f,
            4000.0f, 4000.0f, 4000.0f,
            4000.0f, -4000.0f, 4000.0f,
            -4000.0f, -4000.0f, 4000.0f,

            -4000.0f, 4000.0f, -4000.0f,
            4000.0f, 4000.0f, -4000.0f,
            4000.0f, 4000.0f, 4000.0f,
            4000.0f, 4000.0f, 4000.0f,
            -4000.0f, 4000.0f, 4000.0f,
            -4000.0f, 4000.0f, -4000.0f,

            -4000.0f, -4000.0f, -4000.0f,
            -4000.0f, -4000.0f, 4000.0f,
            4000.0f, -4000.0f, -4000.0f,
            4000.0f, -4000.0f, -4000.0f,
            -4000.0f, -4000.0f, 4000.0f,
            4000.0f, -4000.0f, 4000.0f
    };


    // skybox VAO
    glGenVertexArrays(1, &skyboxVAO);
    glGenBuffers(1, &skyboxVBO);
    glBindVertexArray(skyboxVAO);
    glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)nullptr);
    glEnableVertexAttribArray(0);

    // loading textures
    std::string textName = "sky";
    std::string path = "./textures/" + textName + "_";
    std::string right = path + "right.jpg";
    std::string left = path + "left.jpg";
    std::string top = path + "top.jpg";
    std::string bottom = path + "bottom.jpg";
    std::string back = path + "front.jpg";
    std::string front = path + "back.jpg";
    std::vector<std::string> faces;
    faces.emplace_back(right.c_str());
    faces.emplace_back(left.c_str());
    faces.emplace_back(top.c_str());
    faces.emplace_back(bottom.c_str());
    faces.emplace_back(back.c_str());
    faces.emplace_back(front.c_str());
    cubemapTexture = loadCubemap(faces);
}


void Game::renderSkybox() {
    glDepthMask(GL_FALSE);
    glDepthFunc(GL_LEQUAL);
    glBindVertexArray(skyboxVAO);
    glBindBuffer(GL_ARRAY_BUFFER, skyboxVAO);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDepthFunc(GL_LESS); // set depth function back to default
    glDepthMask(GL_TRUE);

};



void Game::renderBoard() {
    glMatrixMode(GL_MODELVIEW);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    board.render();

    glLoadName(-1);
}

void Game::renderObjects() {
    glMatrixMode(GL_MODELVIEW);

    for (Queen *queen : queens) {
        queen->render();
    }

    glLoadName(-1);
}

void Game::render() {
    glClear(GL_COLOR_BUFFER_BIT);
    glClear(GL_DEPTH_BUFFER_BIT);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);



    renderBoard();

    glEnable(GL_LIGHTING);

    renderObjects();

    glDisable(GL_LIGHTING);

    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);

    glutSwapBuffers();
}


void Game::printBoard(int board[DIMENSION][DIMENSION]) {
    for (int i = 0; i < DIMENSION; i++) {
        for (int j = 0; j < DIMENSION; j++)
            std::cout << board[i][j] << " ";
        std::cout << std::endl;
    }
    std::cout << std::endl;

}

void Game::init() {
    glewInit();
    glClearColor(0.3, 0.3, 0.3, 1.0);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glShadeModel(GL_SMOOTH);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);


    initBoard();

    initObjects();

    initLighting();


    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);

    camera = new Camera();

    soundEngine = createIrrKlangDevice();
}

void Game::initBoard() {
    // Initializing the vertex array
    for (int i = 0; i < BOARD_WIDTH; i++) {
        auto x = -(BOARD_WIDTH / 2) + i;
        for (int j = 0; j < BOARD_HEIGHT; j++) {
            auto z = -(BOARD_HEIGHT / 2) + j;

            board_vertices[((i * BOARD_WIDTH + j) * 3)] = x;
            board_vertices[((i * BOARD_WIDTH + j) * 3) + 1] = 0;
            board_vertices[((i * BOARD_WIDTH + j) * 3) + 2] = z;
        }
    }

    board = Board();
    board.init(board_vertices);
}

void Game::initLighting() {
    glEnable(GL_LIGHTING);

    glEnable(GL_LIGHT0);

    float ambient[] = {0.1f, 0.1f, 0.1f, 0.2f};
    float diffuse[] = {0.8f, 0.8f, 0.8f, 0};
    float specular[] = {0.5f, 0.5f, 0.5f, 0.2f};
    float pos[] = {200.0f, 300.0f, -400.0f, 1.0f};

    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
    glLightfv(GL_LIGHT0, GL_POSITION, pos);
}

void Game::initObjects() {

    for (int i = 0; i < BOARD_WIDTH - 1; ++i) {
        for (int j = 0; j < BOARD_HEIGHT - 1; ++j) {
            queen_positions[i][j] = 0;
        }
    }

    std::srand((time(nullptr)));

    // Generating the queen pieces with a random row and column for each
    for (int i = 0; i < 8; i++) {
        int row, col;
        bool available;

        // Calculating a random row and col and checking for conflict with other pieces' position
        // If so, calculate another random row and col
        do {
            row = (std::rand() % 8);
            col = (std::rand() % 8);
            available = true;
            for (Queen *queen : queens) {
                if (queen->row == row || queen->col == col) {
                    available = false;
                }
            }

        } while (!available);

        // Calculating the position of the square center at the given row and col
        double x = board_vertices[(row * 9 + col) * 3] + 0.5;
        double z = board_vertices[(row * 9 + col) * 3 + 2] + 0.5;
        double y = 0;

        Queen *queen = new Queen(row, col, i, vertex(x, y, z));

        queen_positions[row][col] = 1;

        queens.push_back(queen);
    }
}


void Game::mouseCallback(int button, int state, int x, int y) {
    GLuint selectBuf[BUFFER_CAPACITY];
    GLint hits;
    GLint viewport[4];
    if (button != GLUT_LEFT_BUTTON || state != GLUT_DOWN)
        return;

    glGetIntegerv(GL_VIEWPORT, viewport);
    glSelectBuffer(BUFFER_CAPACITY, selectBuf);
    glRenderMode(GL_SELECT);

    glInitNames();
    glPushName(-1);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluPickMatrix((GLdouble) x, (GLdouble) (viewport[3] - y), 10.0, 10.0, viewport);


    auto width = glutGet(GLUT_WINDOW_WIDTH);
    auto height = glutGet(GLUT_WINDOW_HEIGHT);

    gluPerspective(camera_fov, (float) width / (float) height, 2.0, 500.0);

    gluLookAt(
            15, 10, 20,
            0, 0, 0,
            0, 1, 0);

    glRotatef(camera_theta, 0, 1, 0);


    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glDisable(GL_LIGHTING);

    renderBoard();

    renderObjects();

    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);

    glutSwapBuffers();


    hits = glRenderMode(GL_RENDER);
    select(hits, selectBuf);
}

void Game::select(GLint hits, GLuint buffer[]) {
    GLuint *ptr;


    ptr = (GLuint *) buffer;

    ptr += (hits - 1) * 4 + 3;

    if (selectedQueen != nullptr) {
        for (Quad *quad : board.getQuads()) {
            if (quad->id == *ptr) {
                moveSelectedQueen(quad);
            }
        }
    }

    for (Queen *queen : queens) {
        if ((queen->object).objectId == (*ptr)) {
            selectQueen(queen);
        }
    }
}

void Game::selectQueen(Queen* queen) {
    if (queen->selected) {
        if (queen->inConflict) {
            queen->selected = false;
            queen->setInConflict(true); // back to red
        } else {
            queen->setSelected(false); // back to black
        }

        selectedQueen = nullptr;
    } else {
        for (Queen* currQueen : queens) {
            currQueen->setSelected(false);
            if (currQueen->inConflict) {
                currQueen->setInConflict(true);
            }
        }
        queen->setSelected(true);

        selectedQueen = queen;
    }
}

void Game::moveSelectedQueen(Quad* destQuad) {
    if (movementAllowed(selectedQueen, destQuad)) {
        // mark the destination quad
        for (Queen* queen : queens) {
            if (queen != selectedQueen) {
                queen->resetColor();
            }
        }
        targetQuad = destQuad;
        destQuad->setColor(color4(0.0, 0.0, 1.0, 1.0));
        queen_positions[selectedQueen->row][selectedQueen->col] = 0;
        queen_positions[destQuad->row][destQuad->col] = 1;
        movement_started = true;
        // get movement info
        movementInfo = selectedQueen->move(selectedQueen, destQuad);
        touchTime = glutGet(GLUT_ELAPSED_TIME) / 1000;
        // distance = movementInfo.distance;
        moveLengthX = movementInfo.movePerFrameX;
        moveLengthZ = movementInfo.movePerFrameZ;
    }
}

void Game::timer(int value) {
    if (camera->zoomingIn) {
        if (camera_fov >= 0)
            camera_fov -= 0.5;
    } else if (camera->zoomingOut) {
        if (camera_fov <= 100)
            camera_fov += 0.5;
    }

    if (camera->rotatingLeft) {
        camera_theta += 1.0;
    } else if (camera->rotatingRight) {
        camera_theta -= 1.0;
    }

    int t = glutGet(GLUT_ELAPSED_TIME) / 1000;

    int diffTime = t - touchTime;

    auto width = glutGet(GLUT_WINDOW_WIDTH);
    auto height = glutGet(GLUT_WINDOW_HEIGHT);

    camera->set_camera_config(width, height, camera_theta, camera_fov);
    // check if a queen has started its movement
    if (movement_started) {
        // check if the distance between queen position and the target quad is smaller than 0.1
        if (abs(targetQuad->center_pos.x - selectedQueen->object.getPosition().x) >= 0.1 || abs(targetQuad->center_pos.z - selectedQueen->object.getPosition().z) >= 0.1) {
            selectedQueen->object.setPosition(
                    vertex(selectedQueen->object.getPosition().x + moveLengthX * diffTime * 0.1, selectedQueen->object.getPosition().y,
                           selectedQueen->object.getPosition().z + moveLengthZ * diffTime * 0.1));
        } else {
            // put queen in the target position
            selectedQueen->object.setPosition(
                    vertex(targetQuad->center_pos.x, targetQuad->center_pos.y,
                           targetQuad->center_pos.z));
            selectedQueen->setSelected(false);
            targetQuad->setColor(targetQuad->initColor);
            // play sound when the queen is put on the quad
            soundEngine->play2D("./assets/chess_move.wav", false);
            if (hintEnabled) {
                markConflictingQueens();
            }
            selectedQueen = nullptr;
            targetQuad = nullptr;
            moveLengthX = 0;
            moveLengthZ = 0;
            movement_started = false;
        }
    }

    if (EightQueenSolver::checkForEnd(queen_positions, 0)) {
        // Game end here, but the user can continue moving queens around
    }

    glutTimerFunc(25, timer, value + 1);
    glutPostRedisplay();
}

void Game::reshape(int w, int h) {
    camera->set_camera_config(w, h, camera_theta, camera_fov);
}

void Game::disableConflictingQueens() {
    for (Queen* queen : queens) {
        queen->setInConflict(false);
        if (queen->selected) {
            queen->setSelected(true);
        }
    }
}

void Game::keyboardCallback(int key, int x, int y) {
    switch (key) {
        case GLUT_KEY_UP:
            camera->zoomingIn = true;
            break;

        case GLUT_KEY_DOWN:
            camera->zoomingOut = true;
            break;

        case GLUT_KEY_LEFT:
            camera->rotatingLeft = true;
            break;

        case GLUT_KEY_RIGHT:
            camera->rotatingRight = true;
            break;
        case GLUT_KEY_F11:
            fullScreen = !fullScreen;
            if (fullScreen) {
                glutFullScreen();
            } else {
                glutReshapeWindow(800, 600);
            }
            break;

        default:
            break;
    }
}

void Game::handleKeyboardEvents(unsigned char key, int x, int y) {
    switch (key) {
        case 'h':
        case 'H':
            hintEnabled = !hintEnabled;
            hintEnabled ? markConflictingQueens() : disableConflictingQueens();
            break;
        case 'q':
        case 'Q':
            endgame();
            exit(0);
        default:
            break;
    }
}

void Game::keyboardUpCallback(int key, int x, int y) {
    switch (key) {
        case GLUT_KEY_UP:
            camera->zoomingIn = false;
            break;

        case GLUT_KEY_DOWN:
            camera->zoomingOut = false;
            break;

        case GLUT_KEY_LEFT:
            camera->rotatingLeft = false;
            break;

        case GLUT_KEY_RIGHT:
            camera->rotatingRight = false;
            break;

        default:
            break;
    }
}

bool Game::movementAllowed(Queen *queen, Quad *quad) {
    if (queen == nullptr || quad == nullptr) {
        return false;
    }

    // queen in the quad
    if (queen->row == quad->row && queen->col == quad->col) {
        return false;
    }

    int delta_row = abs(queen->row - quad->row);
    int delta_col = abs(queen->col - quad->col);

    if (delta_col != delta_row && quad->row != queen->row && quad->col != queen->col) {
        return false;
    }

    for (Queen *currQueen : queens) {
        if (currQueen != queen) {
            if ((currQueen->row == quad->row && currQueen->col == quad->col)) {
                return false;
            }

            if (queen->row == quad->row && currQueen->row == quad->row && currQueen->col > std::min(queen->col, quad->col) && currQueen->col <= std::max(queen->col, quad->col)) {
                currQueen->object.setColor(color4(0.0, 0.0, 1.0, 1.0));
                return false;
            }

            if (queen->col == quad->col && currQueen->col == quad->col && currQueen->row > std::min(queen->row, quad->row) && currQueen->row <= std::max(queen->row, quad->row)) {
                currQueen->object.setColor(color4(0.0, 0.0, 1.0, 1.0));

                return false;
            }

            // check diagonal


            if (delta_row == delta_col) {
                int curr_delta_row = abs(quad->row - currQueen->row);
                int curr_delta_col = abs(quad->col - currQueen->col);
                if (curr_delta_row == curr_delta_col && currQueen->row > std::min(queen->row, quad->row) && currQueen->row <= std::max(queen->row, quad->row) && currQueen->col > std::min(queen->col, quad->col) && currQueen->col <= std::max(queen->col, quad->col)) {
                    currQueen->object.setColor(color4(0.0, 0.0, 1.0, 1.0));
                    return false;
                }
            }
        }
    }

    return true;


}

void Game::markConflictingQueens() {
    // resetting all queens' conflicting status
    for (Queen* queen : queens)  {
        queen->setInConflict(false);
    }

    // set color of conflicting queens
    for (Queen* currQueen : queens) {
        for (Queen* otherQueen : queens) {
            if (currQueen != otherQueen) {
                int delta_row = abs(currQueen->row - otherQueen->row);
                int delta_col = abs(currQueen->col - otherQueen->col);
                if (currQueen->row == otherQueen->row || currQueen->col == otherQueen->col || delta_row == delta_col) {
                    otherQueen->setInConflict(true);
                }
            }
        }
    }
}

void Game::endgame() {
    soundEngine->stopAllSounds();
    soundEngine->removeAllSoundSources();
}

int Game::main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(800, 600);
    glutCreateWindow("Eight Queens");

    init();

    glutDisplayFunc(render);
    glutIdleFunc(render);
    glutReshapeFunc(reshape);
    glutSpecialFunc(keyboardCallback);
    glutSpecialUpFunc(keyboardUpCallback);
    glutKeyboardUpFunc(handleKeyboardEvents);
    glutMouseFunc(mouseCallback);
    glutTimerFunc(25, timer, 0);
    glutMainLoop();
}