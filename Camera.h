//
// Created by Sandman on 1/30/2021.
//

#ifndef EIGHTQUEENS_CAMERA_H
#define EIGHTQUEENS_CAMERA_H

#include <GL\glut.h>
#include <glm.hpp>
#include <gtc/type_ptr.hpp>

class Camera {
public:
    Camera();

    Camera(int w, int h, float camera_theta, float fov);

    int width;
    int height;
    float camera_theta;
    float fov;
    bool rotatingLeft;
    bool rotatingRight;
    bool zoomingIn;
    bool zoomingOut;

    void toggleLeftRotation() { rotatingLeft = !rotatingLeft; }

    void toggleRightRotation() { rotatingRight = !rotatingRight; }

    void toggleZoomingIn() { zoomingIn = !zoomingIn; }

    void toggleZoomingOut() { zoomingOut = !zoomingOut; }

    void set_camera_config(int w, int h, float camera_theta, float fov);

};

#endif //EIGHTQUEENS_CAMERA_H
