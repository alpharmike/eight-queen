#ifndef EIGHTQUEENS_MOVEMENTINFO_H
#define EIGHTQUEENS_MOVEMENTINFO_H

struct MovementInfo {
    MovementInfo() {};

    MovementInfo(double distance, double movePerFrameX, double movePerFrameZ) {
        this->distance = distance;
        this->movePerFrameX = movePerFrameX;
        this->movePerFrameZ = movePerFrameZ;
    }

    double distance, movePerFrameX, movePerFrameZ;
};

#endif //EIGHTQUEENS_MOVEMENTINFO_H
