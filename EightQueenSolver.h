//
// Created by Sandman on 1/31/2021.
//

#ifndef EIGHTQUEENS_EIGHTQUEENSOLVER_H
#define EIGHTQUEENS_EIGHTQUEENSOLVER_H

#include <iostream>
#include <vector>

#define DIMENSION 8

namespace EightQueenSolver {
    bool checkForEnd(int queen_positions[DIMENSION][DIMENSION], int col);
    bool isValid(int queen_positions[DIMENSION][DIMENSION], int row, int col);
}

#endif //EIGHTQUEENS_EIGHTQUEENSOLVER_H
