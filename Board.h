//
// Created by Sandman on 1/30/2021.
//

#ifndef EIGHTQUEENS_BOARD_H
#define EIGHTQUEENS_BOARD_H

#define BOARD_WIDTH 9
#define BOARD_HEIGHT 9
#define DIM 9

#include <vector>
#include "Quad.h"

enum QUAD_COLOR {
    BLACK = 0,
    WHITE = 1
};

class Board {
public:
    Board();

    Board(std::vector<Quad *> quads);

    void render();

    void init(GLfloat vertices[]);

    std::vector<Quad *> getQuads() { return quads; }

private:
    std::vector<Quad *> quads;
};

#endif //EIGHTQUEENS_BOARD_H
