#ifndef EIGHTQUEENS_GAME_H
#define EIGHTQUEENS_GAME_H

#include <GL/glut.h>
#include "Camera.h"
#include "Queen.h"
#include <vector>
#include "EightQueenSolver.h"


namespace Game {
    void keyboardCallback(int key, int x, int y); // unsigned char back to int for special func

    void keyboardUpCallback(int key, int x, int y);

    void handleKeyboardEvents(unsigned char key, int x, int y);

    void render();

    void renderBoard();

    void renderObjects();

    void init();

    void initBoard();

    void initLighting();

    void initObjects();

    int main(int argc, char **argv);

    void timer(int value);

    void reshape(int w, int h);

    void mouseCallback(int button, int state, int x, int y);

    void select(GLint hits, GLuint buffer[]);

    void moveSelectedQueen(Quad *destQuad);

    void selectQueen(Queen *queen);

    bool movementAllowed(Queen *queen, Quad *quad);

    void printBoard(int board[DIMENSION][DIMENSION]);

    void markConflictingQueens();

    void disableConflictingQueens();

    void endgame();

    void renderSkybox();

    void initSkybox();

    unsigned int loadCubemap(std::vector<std::string>
                     faces);
};

#endif //EIGHTQUEENS_GAME_H
