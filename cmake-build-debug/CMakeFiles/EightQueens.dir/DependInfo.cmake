# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "D:/OpenGLProjects/EightQueens/Board.cpp" "D:/OpenGLProjects/EightQueens/cmake-build-debug/CMakeFiles/EightQueens.dir/Board.cpp.obj"
  "D:/OpenGLProjects/EightQueens/cmake-build-debug/CMakeFiles/3.16.5/CompilerIdCXX/CMakeCXXCompilerId.cpp" "D:/OpenGLProjects/EightQueens/cmake-build-debug/CMakeFiles/EightQueens.dir/CMakeFiles/3.16.5/CompilerIdCXX/CMakeCXXCompilerId.cpp.obj"
  "D:/OpenGLProjects/EightQueens/Camera.cpp" "D:/OpenGLProjects/EightQueens/cmake-build-debug/CMakeFiles/EightQueens.dir/Camera.cpp.obj"
  "D:/OpenGLProjects/EightQueens/EightQueenSolver.cpp" "D:/OpenGLProjects/EightQueens/cmake-build-debug/CMakeFiles/EightQueens.dir/EightQueenSolver.cpp.obj"
  "D:/OpenGLProjects/EightQueens/Game.cpp" "D:/OpenGLProjects/EightQueens/cmake-build-debug/CMakeFiles/EightQueens.dir/Game.cpp.obj"
  "D:/OpenGLProjects/EightQueens/OBJLoader.cpp" "D:/OpenGLProjects/EightQueens/cmake-build-debug/CMakeFiles/EightQueens.dir/OBJLoader.cpp.obj"
  "D:/OpenGLProjects/EightQueens/Quad.cpp" "D:/OpenGLProjects/EightQueens/cmake-build-debug/CMakeFiles/EightQueens.dir/Quad.cpp.obj"
  "D:/OpenGLProjects/EightQueens/Queen.cpp" "D:/OpenGLProjects/EightQueens/cmake-build-debug/CMakeFiles/EightQueens.dir/Queen.cpp.obj"
  "D:/OpenGLProjects/EightQueens/main.cpp" "D:/OpenGLProjects/EightQueens/cmake-build-debug/CMakeFiles/EightQueens.dir/main.cpp.obj"
  "D:/OpenGLProjects/EightQueens/stb_image.cpp" "D:/OpenGLProjects/EightQueens/cmake-build-debug/CMakeFiles/EightQueens.dir/stb_image.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "D:/OpenGLProjects/glm-master/glm"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
