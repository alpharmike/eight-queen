#ifndef EIGHTQUEENS_QUEEN_H
#define EIGHTQUEENS_QUEEN_H

#include "OBJLoader.h"
#include "MovementInfo.h"
#include "Quad.h"



class Queen {
public:
    Queen();

    Queen(int row, int col, int name, vertex pos);

    void setSelected(bool isSelected);

    void setInConflict(bool isConflicted);

    void resetColor();

    MovementInfo move(Queen* selectedQueen, Quad* targetQuad);

    void render();

    OBJLoader object;
    int row, col;
    bool selected;
    bool inConflict;
};

#endif //EIGHTQUEENS_QUEEN_H
