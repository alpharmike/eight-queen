#include <GL/glew.h>
#include "OBJLoader.h"
#include <iostream>
#include <fstream>
#include <sstream>

OBJLoader::OBJLoader() {
    this->position = vertex(0, 0, 0);
}

OBJLoader::OBJLoader(const char *filename) : OBJLoader() {
    readFile(filename);
    this->initBuffers();
}


void OBJLoader::initBuffers() {
    /**
     * @details: this is responsible for initializing buffers for vertices, normals, faces, colors of an object
     * this generates buffers for each property, and moves data to buffers in a static way
     */
    glGenBuffers(1, &verticesBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, verticesBufferId);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vertex), &vertices[0], GL_STATIC_DRAW);

    glGenBuffers(1, &normalsBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, normalsBufferId);
    glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(vertex), &normals[0], GL_STATIC_DRAW);

    glGenBuffers(1, &facesBufferId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, facesBufferId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, faces.size() * sizeof(GLuint), &faces[0], GL_STATIC_DRAW);

    glGenBuffers(1, &colorsBufferId);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void OBJLoader::setPosition(vertex pos) {
    this->position = pos;
}

void OBJLoader::setColor(color4 color) {
    /**
     * color is set by the user, and it might change during selection and conflicts
     * dynamic draw must be used for further movement of data to the buffer
     */
    std::vector<color4> temp_colors;

    for (int i = 0; i < vertices.size(); i++) {
        temp_colors.push_back(color);
    }

    this->colors = temp_colors;

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, colorsBufferId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, colors.size() * sizeof(color), &colors[0], GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void OBJLoader::render() {
    /**
     * @details: after initializing buffers with the initBuffers function, this functions binds the buffers, meaning that
     * activates each buffer and configures its properties. NULL is used for the pointer as we are using buffers
     */
    glBindBuffer(GL_ARRAY_BUFFER, verticesBufferId);
    glVertexPointer(3, GL_DOUBLE, 0, NULL);

    glBindBuffer(GL_ARRAY_BUFFER, colorsBufferId);
    glColorPointer(4, GL_FLOAT, 0, NULL);

    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_DIFFUSE);
    glColorMaterial(GL_FRONT, GL_SPECULAR);
    glColorMaterial(GL_FRONT, GL_AMBIENT);

    glBindBuffer(GL_ARRAY_BUFFER, normalsBufferId);
    glNormalPointer(GL_DOUBLE, 0, NULL);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, facesBufferId);
    glDrawElements(GL_QUADS, (faces.size() * sizeof(GLuint)) / 4, GL_UNSIGNED_INT, NULL);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

void OBJLoader::readFile(const char *filename) {
    /**
     * @details: reads .obj files for our models (the queen model here) and determines its vertex positions, faces, and normals
     */
    std::string temp;
    std::string line;
    double d_temp;
    double current_v;

    std::ifstream fin(filename);

    if (!fin)
        return;

    while (getline(fin, line)) {
        std::istringstream iss(line);

        if (line.substr(0, 2) == "v ") {
            vertex v;
            iss >> temp >> v.x >> v.y >> v.z;
            this->vertices.push_back(v);
        } else if (line.substr(0, 2) == "f ") {

            iss >> temp;
            iss >> current_v;
            this->faces.push_back(current_v - 1);
            iss.ignore(1);
            iss >> d_temp;
            iss.ignore(1);
            iss >> d_temp;
            iss.ignore(1);
            iss >> current_v;
            this->faces.push_back(current_v - 1);
            iss.ignore(1);
            iss >> d_temp;
            iss.ignore(1);
            iss >> d_temp;
            iss.ignore(1);
            iss >> current_v;
            this->faces.push_back(current_v - 1);
            iss.ignore(1);
            iss >> d_temp;
            iss.ignore(1);
            iss >> d_temp;
            iss.ignore(1);
            iss >> current_v;
            this->faces.push_back(current_v - 1);
            iss.ignore(1);
            iss >> d_temp;
            iss.ignore(1);
            iss >> d_temp;
        } else if (line.substr(0, 3) == "vn ") {

            vertex vn;
            iss >> temp >> vn.x >> vn.y >> vn.z;

            this->normals.push_back(vn);
        }
    }
}